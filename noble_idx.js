let Noble = require('./lib/noble');
let bindings = require('./lib/resolve-bindings')();

module.exports = new Noble(bindings);