//standard packages
const express    = require("express");
const bodyParser = require("body-parser");
const port       = 3000;
const app        = express();


let closeBle     = ''; //the BLE that's close by
let farBle       = {id: null, name: null, color: null, rssi: null};
let newPeep      = '';
const goodCop    = '9379266c3da1400fb77dc8a5737900a7';
const hacker     = 'e35d357940bd4511a36df3892da2c49c';

//peeps stores attributes for the people associated with a beacon ID
let peeps        = [{id: goodCop,name: 'lieutenant hard knocks',color:'black','rssi':-200}
                   ,{id: hacker ,name: 'unknown hacker'        ,color:'white','rssi':-200}]; 


//noble 
let noble = require('./noble_idx');
let RSSI_THRESHOLD = -90;
let EXIT_GRACE_PERIOD = 4000; // milliseconds
let inRange = [];


//alexa skill communication sector
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", (req, res, next) => {
  res.send("ODTUG 2017 GeekAThon app to be used with Amazon Alexa.");
});

app.listen(port, () => console.log(`Listening on ${port}`));

app.route("/hq").post(async (req, res) => {
  let respMessage, endSession;
  const requestType = req.body.request.type;

  console.log('===========================');
  console.log(`Incoming request requestType [${requestType}]`)
  
  //Handle various requests
  if (requestType === "IntentRequest") {
    let intentName = req.body.request.intent.name;
    let intentResolve = await handleIntents(req.body.request);
    respMessage = intentResolve.respMessage;
    endSession = intentResolve.endSession;

  } else if (requestType === "LaunchRequest") {
    console.log(`handling launch request. closeBle.id [${closeBle.id}]`)

    //if the beacon belongs to anyone but the hacker, give his response
    if (closeBle.id !== hacker) {
      respMessage = `Hello ${closeBle.name}. Headquarters has been expecting you.`;
      endSession  = false;
    } else {
      //this beacon belongs to the hacker
      respMessage = `<say-as interpret-as="interjection">uh oh!</say-as>
                    Unidentified ID badge detected. You are not permitted to access this application. A failed hacking attempt
                    notice has been sent out. Please stay here and await your imminent arrest.
                    Remember, <prosody rate="x-slow">Just say no</prosody> to hacking!`
      endSession  = true;
    }
  } else if (requestType === "SessionEndedRequest") {
    respMessage = `Good bye!`;
    endSession  = true;
  } else {
    respMessage = "I don't understand."
    endSession  = false;
  }

  //Send JSON back to Amazon
  res.json({
    version: "1.0",
    response: {
      shouldEndSession: endSession,
      outputSpeech: {
        type: "SSML",
        ssml: "<speak>" + respMessage + "</speak>"
      }
    }
  });

  res.end("done");
});






// ******************************************************* //
// ******************* Intent Handlers ******************* //
// ******************************************************* //
// This is the nuts & bolts of the Alexa skill logic.       //
// The various intents are analyzied and the appropriate   //
// responses are sent back.                                //
function handleIntents(req) {
  let intentName = req.intent.name;
  let responseText;
  console.log("hadleIntents intent name:", intentName);
  return new Promise((resolve, reject) => {
    try {
      if (intentName === "proximity") {
        
        if (farBle.rssi > 90) {
            responseText = `The ${farBle.name} is close by`;
        } else {
            responseText = `The ${farBle.name} is nowhere to be found`;
        }
        resolve({speech: responseText, endSession: false});

      } else if (intentName === "briefing") {
        
        responseText = `${closeBle.name}, congratulations for solving case 23 44 56 11 2 dash 3 dash 3 dash 3. 
                        You still have three outstanding cases.
                        <say-as interpret-as="interjection">dun dun dun!</say-as>This just in: hacking activity has been detected. 
                        A false badge has been used in an attempt to hack into the police system.
                        According to our scanners, the hostile badge is close by. Be on your guard ${closeBle.name}.`;
        console.log('responseText',responseText);
        resolve({respMessage: responseText, endSession: false});

      } else if (intentName === "AMAZON.CancelIntent") {
        
        responseText = `Good bye.`;
        console.log('responseText',responseText);
        resolve({respMessage: responseText, endSession: true});

      }else {
        reject({respMessage: 'rejected', endSession: true});
      }
    } catch (err) {
      console.log('err',err)
      reject ({respMessage: 'there is something wrong', endSession: true})
    }
  });
}












//Start the nobe bluetooth scanning
noble.on('discover', function(peripheral) {
  if (peripheral.rssi < RSSI_THRESHOLD) {
    // ignore
    return;
  }

  var id = peripheral.id;
  var entered = !inRange[id];
  var localName = peripheral.advertisement.localName;

  // A new bluetooth device has been seen
  if (entered) {
    inRange[id] = {
      peripheral: peripheral
    };

    //if the current id is one of the peeps then log. Other bluetooth devices are ignored.
    newPeep = peeps.find(ele => ele.id === id);
    if (newPeep) {
      newPeep.rssi = peripheral.rssi; //newPeep is a pointer to peeps and will update peeps
      console.log('spy discovered',newPeep.color);
      closeBle = Object.keys(peeps).reduce((a,b)=> peeps[a].rssi > peeps[b].rssi ? peeps[a] : peeps[b]);
      farBle = peeps.find(x => x.id !== closeBle.id)
    }
  }
  
  console.log(`closeBle ${closeBle.name} [RSSI: ${closeBle.rssi}].`,`farBle  ${farBle.name} [RSSI: ${farBle.rssi}].`)
  inRange[id].lastSeen = Date.now();
});


noble.on('warning', (msg) => {
	console.log('warning')
});


//periodically check if there are new bluetooth devices
setInterval(function() {
  for (var id in inRange) {
    if (inRange[id].lastSeen < (Date.now() - EXIT_GRACE_PERIOD)) {
      var peripheral = inRange[id].peripheral;
      var localName = peripheral.advertisement.localName;
      //The ble token is out of range. set it to -201 so it won't be considered above
      peeps.find( x => x.id = id).rssi = -201;
      delete inRange[id];
    }
  }
}, EXIT_GRACE_PERIOD / 2);


//check the state of noble
noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    noble.startScanning([], true);
  } else {
    noble.stopScanning();
  }
});